#!/bin/bash

# Matteo Minardi 0000722543

if [ "$#" == "3" ] ; then
    for (( index = 0; index < "$1"; ++index))
    do
        ./../omp-traffic $index $2 $3 > /dev/null 2>&1
    done
    convert -delay 20 omp-traffic*ppm traffic.gif
    rm *.ppm
else
    exit 1;
fi