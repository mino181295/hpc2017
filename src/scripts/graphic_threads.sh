#!/bin/bash

# Matteo Minardi 0000722543


if [ "$#" == "3" ] ; then
    echo -e "THREADS\t\tSERIALE\t\tOMP\t\tMPI"
    for i in 1 2 4 8 16
    do
        echo -ne "$i\t\t"

        ./../traffic $1 $2 $3
        echo -ne "\t"

        OMP_NUM_THREADS=$i ./../omp-traffic $1 $2 $3
        echo -ne "\t"

        mpirun -n $i ./../mpi-traffic $1 $2 $3
        echo -ne "\n"
    done
else
    exit 1;
fi