#!/bin/bash

# Matteo Minardi 0000722543


if [ "$#" == "3" ] ; then
    echo -e "N\t\tSERIALE\t\tOMP\t\tMPI"
    for i in 1 50 100 500 1000 1500 2000 2500
    do
        echo -ne "$i\t\t"

        ./../traffic $1 $2 $i
        echo -ne "\t"

        OMP_NUM_THREADS=$3 ./../omp-traffic $1 $2 $i
        echo -ne "\t"

        mpirun -n $3 ./../mpi-traffic $1 $2 $i
        echo -ne "\n"
    done
else
    exit 1;
fi