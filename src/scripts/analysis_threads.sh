#!/bin/bash

# Matteo Minardi 0000722543

if [ "$1" == "serial" ] ; then
    echo -e "THREADS\t\tt1\t\tt2\t\tt3\t\tt4\t\tt5\t\tt6"
    for i in 1 2 4 8 16
    do
        echo -ne "$i\t"

        for j in 1 2 3 4 5 6
        do
            echo -ne "\t"
           ./../traffic $2 $3 $4
        done
        echo -ne "\n"
    done
    exit 0;
fi

if [ "$1" == "omp" ] ; then
    echo -e "THREADS\t\tt1\t\tt2\t\tt3\t\tt4\t\tt5\t\tt6"
    for i in 1 2 4 8 16
    do
        echo -ne "$i\t"

        for j in 1 2 3 4 5 6
        do
            echo -ne "\t"
            OMP_NUM_THREADS=$i ./../omp-traffic $2 $3 $4
        done
        echo -ne "\n"
    done
    exit 0;
fi

if [ "$1" == "mpi" ] ; then
    echo -e "THREADS\t\tt1\t\tt2\t\tt3\t\tt4\t\tt5\t\tt6"
    for i in 1 2 4 8 16
    do
        echo -ne "$i\t"

        for j in 1 2 3 4 5 6
        do
            echo -ne "\t"
            mpirun -n $i ./../mpi-traffic $2 $3 $4
        done
        echo -ne "\n"
    done
    exit 0;
fi
exit 1;