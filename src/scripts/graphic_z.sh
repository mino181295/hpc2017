#!/bin/bash

# Matteo Minardi 0000722543


if [ "$#" == "3" ] ; then
    echo -e "Z\t\tSERIALE\t\tOMP\t\tMPI"
    for i in 1 10 100 1000
    do
        echo -ne "$i\t\t"

        ./traffic $i $1 $2
        echo -ne "\t"

        OMP_NUM_THREADS=$3 ./omp-traffic $i $1 $2
        echo -ne "\t"

        mpirun -n $3 ./mpi-traffic $i $1 $2
        echo -ne "\n"
    done
else
    exit 1;
fi