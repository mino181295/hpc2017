/****************************************************************************
 *
 * traffic.c - Biham-Middleton-Levine traffic model
 *
 * Written in 2017 by Matteo Minardi <matteo.minardi(at)studio.unibo.it>
 *
 * ---------------------------------------------------------------------------
 *
 * This program implements the Biham-Middleton-Levine traffic model
 * The BML traffic model is a simple three-state 2D cellular automaton
 * over a toroidal square lattice space. Initially, each cell is
 * either empty, or contains a left-to-right (LR) or top-to-bottom
 * (TB) moving vehicle. The model evolves at discrete time steps. Each
 * step is logically divided into two phases: in the first phase only
 * LR vehicles move, provided that the destination cell is empty; in
 * the second phase, only TB vehicles move, again provided that the
 * destination cell is empty.
 *
 * This program is not complete: some functions are missing and must
 * be implemented.
 *
 * Compile with:
 *
 * gcc -std=c99 -Wall -Wpedantic traffic.c -o traffic
 *
 * Run with:
 *
 * ./traffic [nsteps [rho [N]]]
 * 
 * where nsteps is the number of simulation steps to execute, rho is
 * the density of vehicles (probability that a cell is occupied by a
 * vehicle), and N is the grid size.
 *
 ****************************************************************************/
#include "hpc.h"
#include <stdio.h>
#include <stdlib.h>

typedef unsigned char cell_t;

/* Possible values stored in a grid cell */
enum {
    EMPTY = 0,  /* empty cell            */
    LR,         /* left-to-right vehicle */
    TB          /* top-to-bottom vehicle */
};

int IDX(int i, int j, int n){
    int i_index = i < 0 ? n+i : i;
    i_index = i_index > n-1 ? i_index-n : i_index;

    int j_index = j < 0 ? n+j : j;
    j_index = j_index > n-1 ? j_index-n : j_index;

    return (i_index*n + j_index);
} 

/* Move all left-to-right vehicles that are not blocked */
void horizontal_step( cell_t *cur, cell_t *next, int n )
{
    int i, j;
    int C, N, P;
    for(i=0; i<n; i++){                                                     /* Rows */
        for(j=0; j<n; j++){                                                 /* Cols */
            C = IDX(i, j, n);
            N = IDX(i, j+1, n);
            P = IDX(i, j-1, n);
            if (cur[P] == LR && cur[C] == EMPTY){           /* If prec is LR and current is empty move */
                next[C] = LR;
            } else if (cur[C] == LR && cur[N] == EMPTY){    /* If current is LR and next is empty wipe */
                next[C] = EMPTY;
            } else {
                next[C] = cur[C];                            /* The current remains the same */
            }
        }
    }
}

/* Move all top-to-bottom vehicles that are not blocked */
void vertical_step( cell_t *cur, cell_t *next, int n )
{
    int i, j;
    /* The value of the current [C], the next [N] and the precedent [P]
        number of the index (i,j) converted to the array notation for 
        matrix in 1D. */
    int C, N, P;
    for(i=0; i<n; i++){                                                     /* Rows */
        for(j=0; j<n; j++){                                                 /* Cols */
            
            C = IDX(i, j, n);
            N = IDX(i+1, j, n);
            P = IDX(i-1, j, n);

            if (cur[P] == TB && cur[C] == EMPTY){            /* If superior is TB and current is empty move */
                next[C] = TB;
            } else if (cur[C] == TB && cur[N] == EMPTY){     /* If current is TB and inferior is empty wipe */
                next[C] = EMPTY;
            } else {
                next[C] = cur[C];                            /* The current remains the same */
            }
        }
    }
}

/* Initialize |grid| with vehicles with density |rho|. |rho| must be
   in the range [0, 1] (rho = 0 means no vehicle, rho = 1 means that
   every cell is occupied by a vehicle). The direction is chosen with
   equal probability. */
void setup( cell_t* grid, int n, float rho )
{
    int i, j;
    float n_random;
    for(i=0; i<n; i++){
        for(j=0; j<n; j++){
            n_random = (float)rand()/(float)RAND_MAX;
            if (n_random < rho){
                if (rand() % 2){
                    grid[i*n + j] = LR;
                } else {
                    grid[i*n + j] = TB;
                }
            } else {
                grid[i*n + j] = EMPTY;
            }
        }
    }
}

/* Dump |grid| as a PPM (Portable PixMap) image written to file
   |filename|. LR vehicles are shown as blue pixels, while TB vehicles
   are shown in red. Empty cells are white. */
void dump( const cell_t *grid, int n, const char* filename )
{
    int i, j;
    FILE *out = fopen( filename, "w" );
    if ( NULL == out ) {
        printf("Cannot create \"%s\"\n", filename);
        abort();
    }
    fprintf(out, "P6\n");
    fprintf(out, "%d %d\n", n, n);
    fprintf(out, "255\n");
    for (i=0; i<n; i++) {
        for (j=0; j<n; j++) {
            switch( grid[i*n + j] ) {
            case EMPTY:
                fprintf(out, "%c%c%c", 255, 255, 255);
                break;
            case TB:
                fprintf(out, "%c%c%c", 0, 0, 255);
                break;
            case LR:
                fprintf(out, "%c%c%c", 255, 0, 0);
                break;
            default:
                printf("Error: unknown cell state %u\n", grid[IDX(i,j, n)]);
                abort();
            }
        }
    }
    fclose(out);
}

#define BUFLEN 256

int main( int argc, char* argv[] )
{
    cell_t *cur, *next;
    char buf[BUFLEN];
    int s, N = 256, nsteps = 512;
    float rho = 0.2;
    double tstart, tend;

    if ( argc > 4 ) {
        printf("Usage: %s [nsteps [rho [N]]]\n", argv[0]);
        return -1;
    }

    if ( argc > 1 ) {
        nsteps = atoi(argv[1]);
    }

    if ( argc > 2 ) {
        rho = atof(argv[2]);
    }

    if ( argc > 3 ) {
        N = atoi(argv[3]);
    }

    const size_t size = N*N*sizeof(cell_t);

    /* Allocate grids */
    cur = (cell_t*)malloc(size);
    next = (cell_t*)malloc(size);

    setup(cur, N, rho);
    tstart = hpc_gettime();
    for (s=0; s<nsteps; s++) {
        horizontal_step(cur, next, N);
        vertical_step(next, cur, N);
    }
    tend = hpc_gettime();
    fprintf(stderr, "%f", tend - tstart);
    /* dump last state */
    snprintf(buf, BUFLEN, "traffic-%05d.ppm", s);
    dump(cur, N, buf);

    /* Free memory */
    free(cur);
    free(next);
    return 0;
}
