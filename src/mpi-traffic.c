/****************************************************************************
 *
 * mpi-traffic.c - Biham-Middleton-Levine traffic model
 *
 * Written in 2017 by Matteo Minardi <matteo.minardi(at)studio.unibo.it>
 *
 * ---------------------------------------------------------------------------
 *
 * This program implements the Biham-Middleton-Levine traffic model
 * The BML traffic model is a simple three-state 2D cellular automaton
 * over a toroidal square lattice space. Initially, each cell is
 * either empty, or contains a left-to-right (LR) or top-to-bottom
 * (TB) moving vehicle. The model evolves at discrete time steps. Each
 * step is logically divided into two phases: in the first phase only
 * LR vehicles move, provided that the destination cell is empty; in
 * the second phase, only TB vehicles move, again provided that the
 * destination cell is empty.
 * 
 * Compile with:
 *
 * mpicc -std=c99 -Wall -Wpedantic mpi-traffic.c -lrt -o mpi-traffic
 *
 * Run with:
 *
 * mpirun -n X ./omp-traffic [nsteps [rho [N]]]
 * 
 * where X is the num of processes, nsteps is the number of simulation 
 * steps to execute, rho is the density of vehicles (probability that
 * a cell is occupied by a vehicle), and N is the grid size.
 *
 ****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>

typedef unsigned char cell_t;
const int HALO = 1;

/* Possible values stored in a grid cell */
enum
{
    EMPTY = 0, /* empty cell            */
    LR,        /* left-to-right vehicle */
    TB         /* top-to-bottom vehicle */
};

int IDX(int i, int j, int r, int c)
{
    int i_index = i < 0 ? r + i : i;
    i_index = i_index > r - 1 ? i_index - r : i_index;

    int j_index = j < 0 ? c + j : j;
    j_index = j_index > c - 1 ? j_index - c : j_index;

    return (i_index * c + j_index);
}

/* Move all left-to-right vehicles that are not blocked */
void horizontal_step(cell_t *cur, cell_t *next, int r, int c)
{
    int i, j;
    int C, N, P;
    for (i = 0; i < r; i++)
    { /* Rows */
        for (j = 0; j < c; j++)
        { /* Cols */
            C = IDX(i, j, r, c);
            N = IDX(i, j + 1, r, c);
            P = IDX(i, j - 1, r, c);
            if (cur[P] == LR && cur[C] == EMPTY)
            { /* If prec is LR and current is empty move */
                next[C] = LR;
            }
            else if (cur[C] == LR && cur[N] == EMPTY)
            { /* If current is LR and next is empty wipe */
                next[C] = EMPTY;
            }
            else
            {
                next[C] = cur[C]; /* The current remains the same */
            }
        }
    }
}

/* Move all top-to-bottom vehicles that are not blocked */
void vertical_step(cell_t *cur, cell_t *next, int r, int c)
{
    int i, j;
    /* The value of the current [C], the next [N] and the precedent [P]
        number of the index (i,j) converted to the array notation for 
        matrix in 1D. */
    int C, N, P;
    for (i = 0; i < r; i++)
    { /* Rows */
        for (j = 0; j < c; j++)
        { /* Cols */

            C = IDX(i, j, r, c);
            N = IDX(i + 1, j, r, c);
            P = IDX(i - 1, j, r, c);

            if (cur[P] == TB && cur[C] == EMPTY)
            { /* If superior is TB and current is empty move */
                next[C] = TB;
            }
            else if (cur[C] == TB && cur[N] == EMPTY)
            { /* If current is TB and inferior is empty wipe */
                next[C] = EMPTY;
            }
            else
            {
                next[C] = cur[C]; /* The current remains the same */
            }
        }
    }
}

/* Initialize |grid| with vehicles with density |rho|. |rho| must be
   in the range [0, 1] (rho = 0 means no vehicle, rho = 1 means that
   every cell is occupied by a vehicle). The direction is chosen with
   equal probability. */
void setup(cell_t *grid, int n, float rho)
{
    int i, j;
    float n_random;
    for (i = 0; i < n; i++)
    {
        for (j = 0; j < n; j++)
        {
            n_random = (float)rand() / (float)RAND_MAX;
            if (n_random < rho)
            {
                if (rand() % 2)
                {
                    grid[i * n + j] = LR;
                }
                else
                {
                    grid[i * n + j] = TB;
                }
            }
            else
            {
                grid[i * n + j] = EMPTY;
            }
        }
    }
}

/* Dump |grid| as a PPM (Portable PixMap) image written to file
   |filename|. LR vehicles are shown as blue pixels, while TB vehicles
   are shown in red. Empty cells are white. */
void dump(const cell_t *grid, int n, const char *filename)
{
    int i, j;
    FILE *out = fopen(filename, "w");
    if (NULL == out)
    {
        printf("Cannot create \"%s\"\n", filename);
        abort();
    }
    fprintf(out, "P6\n");
    fprintf(out, "%d %d\n", n, n);
    fprintf(out, "255\n");
    for (i = 0; i < n; i++)
    {
        for (j = 0; j < n; j++)
        {
            switch (grid[i * n + j])
            {
            case EMPTY:
                fprintf(out, "%c%c%c", 255, 255, 255);
                break;
            case TB:
                fprintf(out, "%c%c%c", 0, 0, 255);
                break;
            case LR:
                fprintf(out, "%c%c%c", 255, 0, 0);
                break;
            default:
                printf("Error: unknown cell state %u\n", grid[IDX(i, j, n, n)]);
                abort();
            }
        }
    }
    fclose(out);
}

#define BUFLEN 256

int main(int argc, char *argv[])
{
    cell_t *cur, *next;
    char buf[BUFLEN];

    int s, N = 256, nsteps = 512;
    float rho = 0.2;

    double tstart, tend;

    int my_rank, comm_sz;
    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
    MPI_Comm_size(MPI_COMM_WORLD, &comm_sz);

    if (0 == my_rank && argc > 4)
    {
        printf("Usage: %s [nsteps [rho [N]]]\n", argv[0]);
        MPI_Abort(MPI_COMM_WORLD, -1);
    }
    if (argc > 1)
    {
        nsteps = atoi(argv[1]);
    }
    if (argc > 2)
    {
        rho = atof(argv[2]);
    }
    if (argc > 3)
    {
        N = atoi(argv[3]);
    }
    /* compute the rank of the next and previous process on the
       chain. These will be used to exchange the boundary */
    const int next_rank = (my_rank + 1) % comm_sz;
    const int prev_rank = (my_rank - 1 + comm_sz) % comm_sz;
    /* initialization done by the master; 
       Only the master has the complete matrix allocated
       The other workers has a N / comm_sz part of it */
    if (0 == my_rank)
    {
        /* Allocate grids */
        const size_t size = N * N * sizeof(cell_t);
        cur = (cell_t *)malloc(size);
        next = (cell_t *)malloc(size);

        setup(cur, N, rho);
    }
    /* size of each local domain; it compute the rows (allows not multiple
       of comm_sz and adds the 2 ghost rows (bottom and top) since it must 
       compute the horizontal and vertical steps*/
    int nrows = N / comm_sz;
    if (0 == my_rank && comm_sz > 1)
        nrows = nrows + N % comm_sz;

    const int local_n = N * nrows + 2 * N * HALO;
    /* setup of the arrays that contains the counts of the
       elements to be scattered to each MPI process */
    int *sendcnts = (int *)malloc(comm_sz * sizeof(int));
    int *displs = (int *)malloc(comm_sz * sizeof(int));
    for (int i = 0; i < comm_sz; ++i)
    {
        if (i == 0)
        {
            sendcnts[i] = N / comm_sz + N % comm_sz;
            sendcnts[i] *= N;
            displs[i] = 0;
        }
        else
        {
            sendcnts[i] = N / comm_sz;
            sendcnts[i] *= N;
            displs[i] = displs[i - 1] + sendcnts[i - 1];
        }
    }
    /* allocate local buffers */
    cell_t *local_cur = (cell_t *)calloc(local_n, sizeof(cell_t));
    cell_t *local_next = (cell_t *)calloc(local_n, sizeof(cell_t));

    tstart = MPI_Wtime();

    /* Distribute the domain to the processes. Each process must
       receive |N*nrows| elements of type MPI_UNSIGNED_CHAR; every
       element sent has it's own sendcnts and displs value */
    MPI_Scatterv(cur,                  /* sendbuf      */
                 sendcnts,             /* sendcount    */
                 displs,               /* displs    */
                 MPI_UNSIGNED_CHAR,    /* datatype     */
                 &local_cur[N * HALO], /* recvbuf      */
                 N * nrows,            /* recvcount    */
                 MPI_UNSIGNED_CHAR,    /* datatype     */
                 0,                    /* root         */
                 MPI_COMM_WORLD);

    /* Evolve the system */
    for (s = 0; s < nsteps; s++)
    {
        /* send bottom boundary */
        MPI_Sendrecv(&local_cur[local_n - 2 * N * HALO], /* sendbuf      */
                     N * HALO,                           /* sendcount    */
                     MPI_UNSIGNED_CHAR,                  /* datatype     */
                     next_rank,                          /* dest         */
                     0,                                  /* sendtag      */
                     &local_cur[0],                      /* recvbuf      */
                     N * HALO,                           /* recvcount    */
                     MPI_UNSIGNED_CHAR,                  /* datatype     */
                     prev_rank,                          /* source       */
                     0,                                  /* recvtag      */
                     MPI_COMM_WORLD,
                     MPI_STATUS_IGNORE);

        /* send top boundary */
        MPI_Sendrecv(&local_cur[N * HALO],           /* sendbuf      */
                     N * HALO,                       /* sendcount    */
                     MPI_UNSIGNED_CHAR,              /* datatype     */
                     prev_rank,                      /* dest         */
                     0,                              /* sendtag      */
                     &local_cur[local_n - N * HALO], /* recvbuf      */
                     N * HALO,                       /* recvcount    */
                     MPI_UNSIGNED_CHAR,              /* datatype     */
                     next_rank,                      /* source       */
                     0,                              /* recvtag      */
                     MPI_COMM_WORLD,
                     MPI_STATUS_IGNORE);
        /* each process computes the next state of the system */
        horizontal_step(local_cur, local_next, local_n / N, N);
        vertical_step(local_next, local_cur, local_n / N, N);
    }
    /* Gather at the root with the proper dimensions */
    MPI_Gatherv(&local_cur[N * HALO], /* sendbuf      */
                N * nrows,            /* sendcount    */
                MPI_UNSIGNED_CHAR,    /* datatype     */
                cur,                  /* recvbuf      */
                sendcnts,             /* recvcount    */
                displs,               /* displs       */
                MPI_UNSIGNED_CHAR,    /* datatype     */
                0,                    /* root         */
                MPI_COMM_WORLD);

    if (0 == my_rank)
    {
        tend = MPI_Wtime();
        fprintf(stderr, "%f", tend - tstart);
        /* dump last state */
        snprintf(buf, BUFLEN, "mpi-traffic-%05d.ppm", s);
        dump(cur, N, buf);

        free(cur);
        free(next);
    }
    free(local_cur);
    free(local_next);

    MPI_Finalize();

    return 0;
}
