#Guida all'utilizzo

##Compilazione

Per compilare questo i file � sufficiente spostarsi in src/ e lanciare il
comando make. 

E' possibile utilizzare il comando make con le seguenti parole chiave:
* clean: per rimuovere i file compilati e le immagini.
* omp, mpi: per compilare rispettivamente soltanto i file di quel tipo.
* cleanimg: per eliminiare le immagini create dalle eventuali esecuzioni.

##Scripts

Nella cartella src/scripts/ sono presenti alcuni script usati per la graficazione 
dei dati durante la redazione della relazione. 
Verranno brevemente spiegati e verr� inoltre indicata la lista dei parametri di ognuno.

###analysis_thread.sh
Questo script consente di visualizzare, per uno specifico programma
(mpi, omp, serial), il tempo impiegato con un numero diverso di thread e un numero
multiplo di prove (6). 

`./analysis_threads.sh [program] [nsteps] [rho] [N]`

###graphic_n.sh
Questo script consente di visualizzare, dato un numero fisso di nsteps
come varia a fronte del cambiamento di N. 

`./graphic_n.sh [nsteps] [rho] [nthreads]`

###graphic_z.sh
Questo script consente di visualizzare, dato un numero fisso di N
come varia il tempo a fronte del cambiamento di nsteps. 

`./graphic_z.sh [rho] [N] [nthreads]`

###graphic_threads.sh
Questo script consente di visualizzare, dato un problema BML fisso
come variano i tempi al crescere del numero di threads. 

`./graphic_threads.sh [nsteps] [rho] [N]`


###traffic.sh

Questo script genera una gif (utilizzando la versione seriale)
formata dagli nsteps passati come parametro.

`./traffic.sh [nsteps] [rho] [N]`

###omp-traffic.sh

Questo script genera una gif (utilizzando la versione seriale)
formata dagli nsteps passati come parametro.

`./omp-traffic.sh [nsteps] [rho] [N]`


